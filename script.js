const gap = 16;

const carousel = document.getElementById("carousel"),
    content = document.getElementById("content"),
    next = document.getElementById("next"),
    prev = document.getElementById("prev");

next.addEventListener("click", e => {
    carousel.scrollBy(width + gap, 0);
    if (carousel.scrollWidth !== 0) {
        prev.style.display = "flex";
    }
    if (content.scrollWidth - width - gap <= carousel.scrollLeft + width) {
        next.style.display = "none";
    }
});
prev.addEventListener("click", e => {
    carousel.scrollBy(-(width + gap), 0);
    if (carousel.scrollLeft - width - gap <= 0) {
        prev.style.display = "none";
    }
    if (!content.scrollWidth - width - gap <= carousel.scrollLeft + width) {
        next.style.display = "flex";
    }
});

let width = carousel.offsetWidth;
window.addEventListener("resize", e => (width = carousel.offsetWidth));



const pointer = (e)=>{
    e.parentElement.querySelector('.process__description').classList.toggle('d-none')
}
const closeDesc = (e)=>{
    e.parentElement.classList.add('d-none')
}




// Get the modal element
const modal = document.getElementById("myModal");

// Get the button that opens the modal
// Get the <span> element that closes the modal
const span = document.getElementsByClassName("close")[0];

// When the user clicks the button, open the modal
const openModal = function() {
    modal.style.display = "flex";
}

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
    modal.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
    if (event.target === modal) {
        modal.style.display = "none";
    }
}




const slider = document.getElementById('slider');
const sliderValue = document.getElementById('slider-value');

// Set the initial value of the slider value element
sliderValue.innerHTML = slider.value;

// Update the slider value element when the slider is moved
slider.addEventListener('input', function() {
    sliderValue.innerHTML = this.value;
});


const modalPrevBtn = document.getElementById('prev-btn')
const modalNextBtn = document.querySelector('#next-btn')
const modalContainer = document.getElementById('steps')
const stepList = ["where","area","aim","when","your-num","thanks"]
console.log('heeey')
let step = 0
modalNextBtn.onclick = ()=>{
    const prev = document.getElementById(stepList[step])
    const next = document.getElementById(stepList[step+1])
    step++
    prev.style.display = "none";
    next.style.display = "block";
}
modalPrevBtn.onclick = ()=>{
    const prev = document.getElementById(stepList[step])
    const next = document.getElementById(stepList[step-1])
    step--
    prev.style.display = "none";
    next.style.display = "block";
}


(function() {
    // https://dashboard.emailjs.com/admin/account
    // public key
    emailjs.init('qD7CZ7n1WvXqEb3GR');
})();


window.onload = function() {
    document.getElementById('contact-form').addEventListener('submit', function(event) {
        event.preventDefault();

        // service id and template id

        emailjs.send("service_m9jpkrq","template_jj54xxn",{
            message: this.message.value,
            to_name: this.email.value,
        })
            .then(function() {
                alert('Спасибо Ваш запрос отправлен!');
            }, function(error) {
                alert('Ошибка! Попробуйте позднее')
            });
        this.message.value="";
    });
}
